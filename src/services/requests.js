/**
 * Light `fetch` wrapper
 * @param  {...any} params fetch params
 * @returns {Promise<Object>|Promise<Error>}
 */
export const get = (...params) =>
  fetch(...params).then((response) => {
    if (response.status >= 200 && response.status <= 299) {
      return response.json();
    } else {
      throw Error(response.statusText);
    }
  });
