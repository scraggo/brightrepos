const BASE_URL = "https://api.github.com";
export const REPOS_URL = `${BASE_URL}/search/repositories`;
export const REPO_DETAILS_URL = `${BASE_URL}/repos`;

/**
 * @param {string} owner
 * @param {string} repoName
 * @returns {string} commits url endpoint
 */
export const makeCommitsURL = (owner, repoName) =>
  `${REPO_DETAILS_URL}/${owner}/${repoName}/commits`;
