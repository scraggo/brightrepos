import { get } from "../requests";

import { HEADERS } from "./config/headers";
import { REPOS_URL } from "./config/endpoints";

// import MOCK_REPOS from "./mocks/repos.json";

const MIN_STAR_COUNT = 40000;

const getStarredReposURL = () => {
  const starredReposURL = new URL(REPOS_URL);
  starredReposURL.searchParams.append("q", `stars:>=${MIN_STAR_COUNT}`);
  starredReposURL.searchParams.append("sort", "stars");
  starredReposURL.searchParams.append("order", "desc");
  starredReposURL.searchParams.append("per_page", 100);
  return starredReposURL.href;
};

export const getStarredRepos = () => {
  const starredReposURL = getStarredReposURL();
  return get(starredReposURL, HEADERS);
};
