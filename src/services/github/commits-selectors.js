export const commitIDSelector = (data) => data.node_id;
export const commitDateSelector = (data) => data.commit.author.date;
export const commitMessageSelector = (data) => data.commit.message;

const commitAuthorSelector = (data) => data.commit.author;
const commitCommitterSelector = (data) => data.commit.committer;

export const commitAuthorNameSelector = (data) => {
  const author = commitAuthorSelector(data);
  const committer = commitCommitterSelector(data);
  if (author) return author.name;
  if (committer) return committer.name;
  return "";
};
