import { getISODate1DayAgo } from "../../utils/date";
import { get } from "../requests";

// import MOCK_COMMITS from "./mocks/commits.json";
import { HEADERS } from "./config/headers";
import { makeCommitsURL } from "./config/endpoints";

const getCommitsURL = (owner, repoName) => {
  const url = makeCommitsURL(owner, repoName);
  const commitsURL = new URL(url);
  commitsURL.searchParams.append("since", getISODate1DayAgo());
  commitsURL.searchParams.append("per_page", 100);
  // commitsURL.searchParams.append("sort", "stars");
  // commitsURL.searchParams.append("order", "desc");
  return commitsURL.href;
};

export const getCommitsForRepo = (owner, repoName) => {
  const url = getCommitsURL(owner, repoName);
  return get(url, HEADERS);
  // return Promise.resolve(MOCK_COMMITS);
};
