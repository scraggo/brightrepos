export const reposSelector = (response) => response.items;

export const repoIDSelector = (repo) => repo.id;
export const repoNameSelector = (repo) => repo.name;
export const repoOwnerSelector = (repo) => repo.owner.login;
export const repoURLSelector = (repo) => repo.html_url;
export const repoStarCountSelector = (repo) => repo.stargazers_count;
