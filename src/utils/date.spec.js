import { getISODate1DayAgo } from "./date";

describe("date utils", () => {
  describe("getISODate1DayAgo", () => {
    it("gets date 1 day ago as ISO string", () => {
      jest
        .spyOn(global.Date, "now")
        .mockImplementationOnce(() => "2019-05-14T11:01:58.135Z");

      expect(getISODate1DayAgo()).toEqual("2019-05-13T11:01:58.135Z");
    });
  });
});
