// @ts-check

/**
 * @param {Date} dateObject
 * @returns {string} format: 2011-10-05T14:48:00.000Z
 */
export const getISODate = (dateObject) => dateObject.toISOString();

/**
 * @param {number} [days]
 * @returns {Date}
 */
export const subtractDaysFromNow = (days = 1) => {
  const event = new Date(Date.now());
  event.setDate(event.getDate() - days);
  return event;
};

/**
 * @returns {string} 24 hours ago ISO string
 */
export const getISODate1DayAgo = () => {
  const yesterday = subtractDaysFromNow(1);
  return getISODate(yesterday);
};
