import {
  repoIDSelector,
  reposSelector,
} from "../../services/github/repos-selectors";

import RepoCard from "../RepoCard";
import "./ReposList.css";

function ReposList({ repoData, setRepoDetails }) {
  const repos = reposSelector(repoData);
  return (
    <div className="ReposList">
      {repos.map((repo) => (
        <RepoCard
          key={repoIDSelector(repo)}
          setRepoDetails={setRepoDetails}
          repo={repo}
        />
      ))}
    </div>
  );
}

export default ReposList;
