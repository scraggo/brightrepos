import { useEffect, useState } from "react";

import { getCommitsForRepo } from "../../services/github/commits";
import {
  repoNameSelector,
  repoOwnerSelector,
} from "../../services/github/repos-selectors";

import RepoDetailsView from "./RepoDetailsView";

function RepoDetails({ handleClose, repoData }) {
  const repoName = repoNameSelector(repoData);
  const repoOwner = repoOwnerSelector(repoData);

  const [repoCommits, setRepoCommits] = useState();
  const [repoCommitsError, setRepoCommitsError] = useState(null);

  useEffect(() => {
    getCommitsForRepo(repoName, repoOwner)
      .then((commits) => setRepoCommits(commits))
      .catch((err) => setRepoCommitsError(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <RepoDetailsView
      handleClose={handleClose}
      repoCommits={repoCommits}
      repoCommitsError={repoCommitsError}
      repoData={repoData}
    />
  );
}

export default RepoDetails;
