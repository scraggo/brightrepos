import { shallow } from "enzyme";

import MOCK_REPOS from "../../services/github/mocks/repos";
import MOCK_COMMITS from "../../services/github/mocks/commits";
import { reposSelector } from "../../services/github/repos-selectors";
import RepoDetailsView from "./RepoDetailsView";

const minProps = {
  handleClose: jest.fn(),
  repoCommits: MOCK_COMMITS,
  // repoCommitsError
  repoData: reposSelector(MOCK_REPOS)[0],
};

describe("RepoDetailsView", () => {
  it("matches snapshot with existing commits", () => {
    const wrapper = shallow(<RepoDetailsView {...minProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("displays Loading... if loading", () => {
    const wrapper = shallow(
      <RepoDetailsView {...minProps} repoCommits={undefined} />
    );
    const loading = wrapper.findWhere(
      (elem) => elem.type() === "div" && elem.text() === "Loading..."
    );
    expect(loading).toHaveLength(1);
  });
});
