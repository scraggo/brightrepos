import {
  commitAuthorNameSelector,
  commitDateSelector,
  commitIDSelector,
  commitMessageSelector,
} from "../../services/github/commits-selectors";
import {
  repoNameSelector,
  repoURLSelector,
} from "../../services/github/repos-selectors";

import "./RepoDetails.css";

const errorMessageDisplay = (err) => {
  if (err.message === "Not Found") {
    return <div>There are no commits to display.</div>;
  }

  return (
    <div className="error-message">
      There was an error getting the commit list: {err.message}
    </div>
  );
};

function RepoDetailsView({
  handleClose,
  repoCommits,
  repoCommitsError,
  repoData,
}) {
  const repoName = repoNameSelector(repoData);
  const repoURL = repoURLSelector(repoData);

  const showError = Boolean(repoCommitsError);
  const isLoading = !repoCommits && !showError;
  const isReady = repoCommits && !showError;

  return (
    <div className="RepoDetails">
      <header>
        <button onClick={handleClose}>{"<"}</button>
        <h3 className="header-text">
          <a href={repoURL} target="blank" rel="noopener noreferrer">
            {repoName}
          </a>
        </h3>{" "}
        <span className="header-text">commits in the last 24 hours</span>
      </header>
      {isLoading ? <div>Loading...</div> : null}
      {isReady ? (
        <div>
          {repoCommits.map((data) => {
            const author = commitAuthorNameSelector(data);
            const date = commitDateSelector(data);
            const message = commitMessageSelector(data);
            return (
              <div key={commitIDSelector(data)} className="commit">
                <div className="commit-header">
                  {author} {date}
                </div>
                <div className="commit-message">{message}</div>
              </div>
            );
          })}
        </div>
      ) : null}
      {showError ? errorMessageDisplay(repoCommitsError) : null}
    </div>
  );
}

export default RepoDetailsView;
