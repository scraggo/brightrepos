import {
  repoNameSelector,
  repoURLSelector,
  repoStarCountSelector,
} from "../../services/github/repos-selectors";

import "./RepoCard.css";

const getRepoURLDisplay = (repoURL) => {
  if (repoURL.length <= 40) return repoURL;
  return `${repoURL.slice(0, 40)}...`;
};

function RepoCard({ setRepoDetails, repo }) {
  const repoName = repoNameSelector(repo);
  const repoURL = repoURLSelector(repo);
  const repoStarCount = repoStarCountSelector(repo);

  const onShowDetails = () => setRepoDetails(repo);

  return (
    <div className="RepoCard">
      <h2>{repoName}</h2>
      <div>{repoStarCount} stars</div>
      <div>
        <a href={repoURL} target="blank" rel="noopener noreferrer">
          {getRepoURLDisplay(repoURL)}
        </a>
      </div>
      <div className="btn-container">
        <button onClick={onShowDetails}>Commits</button>
      </div>
    </div>
  );
}

export default RepoCard;
