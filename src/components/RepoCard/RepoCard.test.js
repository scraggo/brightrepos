import { shallow } from "enzyme";

import MOCK_REPOS from "../../services/github/mocks/repos";
import { reposSelector } from "../../services/github/repos-selectors";
import RepoCard from "./index";

const minProps = {
  setRepoDetails: jest.fn(),
  repo: reposSelector(MOCK_REPOS)[0],
};

describe("RepoCard", () => {
  it("matches snapshot", () => {
    const wrapper = shallow(<RepoCard {...minProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
