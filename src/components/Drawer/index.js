import clsx from "clsx";

import "./Drawer.css";

function Drawer({ children, show }) {
  return (
    <>
      {show ? <div className="Backdrop" /> : null}
      <div className={clsx("Drawer", show && "open")}>
        {show ? children : null}
      </div>
    </>
  );
}

export default Drawer;
