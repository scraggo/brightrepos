import PropTypes from "prop-types";
import Drawer from "../Drawer";
import ReposList from "../ReposList";
import RepoDetails from "../RepoDetails";

import "./App.css";
import logo from "./logo.svg";

function AppView({ repoData, repoDataError, repoDetails, setRepoDetails }) {
  const showError = Boolean(repoDataError);
  const isLoading = !repoData && !showError;
  const isReady = repoData && !showError;
  const showRepoDetails = Boolean(repoDetails);

  const handleDetailsClose = () => setRepoDetails(false);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <span className="text">brightrepos</span>
      </header>
      <Drawer show={showRepoDetails}>
        <RepoDetails handleClose={handleDetailsClose} repoData={repoDetails} />
      </Drawer>
      {isLoading ? <div>Loading...</div> : null}
      {isReady ? (
        <ReposList setRepoDetails={setRepoDetails} repoData={repoData} />
      ) : null}
      {showError ? (
        <div className="error-message">
          There was an error getting repo data: {repoDataError.message}
        </div>
      ) : null}
    </div>
  );
}

AppView.propTypes = {
  repoData: PropTypes.shape({}),
  repoDataError: PropTypes.any,
  repoDetails: PropTypes.arrayOf(PropTypes.shape({})),
  setRepoDetails: PropTypes.func.isRequired,
};

export default AppView;
