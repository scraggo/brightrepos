import { useEffect, useState } from "react";

import { getStarredRepos } from "../../services/github/repos";
import AppView from "./AppView";

function App() {
  const [repoData, setRepoData] = useState();
  const [repoDataError, setRepoDataError] = useState();
  const [repoDetails, setRepoDetails] = useState();

  useEffect(() => {
    getStarredRepos()
      .then((repoCallResponse) => setRepoData(repoCallResponse))
      .catch((err) => setRepoDataError(err));
  }, []);

  return (
    <AppView
      repoData={repoData}
      repoDataError={repoDataError}
      repoDetails={repoDetails}
      setRepoDetails={setRepoDetails}
    />
  );
}

export default App;
