import { shallow } from "enzyme";
import AppView from "./AppView";

const minProps = {
  // repoData
  // repoDataError
  // repoDetails
  setRepoDetails: jest.fn(),
};

describe("AppView", () => {
  it("renders header, Drawer, Loading... on initial load", () => {
    const wrapper = shallow(<AppView {...minProps} />);
    expect(wrapper).toHaveLength(1);
    expect(wrapper.find("header")).toHaveLength(1);
    expect(wrapper.find("Drawer")).toHaveLength(1);
    const loading = wrapper.findWhere(
      (elem) => elem.type() === "div" && elem.text() === "Loading..."
    );
    expect(loading).toHaveLength(1);
  });
});
