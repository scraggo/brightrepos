# Architecture

## File structure

- public
- src - contains top-level app concerns
  - components - each React component has its own directory. Tests and styling are included
  - services - includes a light wrapper for `fetch`
    - github
      - config - holds endpoints and headers
      - mocks - used in testing (and development phase)
  - utils - for date manipulation

## Data flow

- The top-level `App` fetches the starred repo data. This data is available to all components via props passing.
- The `RepoDetails` component fetches commits for a repo when it's mounted (i.e. "lazy loading")

## Components

- `App` - fetches and holds app state including: the fetched repos and a selected repo for viewing its commits
- `AppView` holds children UI components and passes data down
- `Drawer` holds `RepoDetails`
- `RepoList` renders `RepoCards`

"View" components are preferred as children of `useEffect`, data fetching components. It is much easier to test this way.
