# Testing

## Approaches

- prefer integration over unit tests
- mock any API responses. should be simple as they're passed as props in "View" components.
- mock "impure" `Date` api
- use `enzyme`
- test all permutations of props
- snapshot tests are quick
