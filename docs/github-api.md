# Github API

## Search API

<https://docs.github.com/en/rest/reference/search>

The Search API helps you search for the specific item you want to find.

<https://docs.github.com/en/rest/reference/search#constructing-a-search-query>

Each endpoint in the Search API uses query parameters to perform searches on GitHub.

- key: `q`
- value: `{string}`
  - Search terms, like `GitHub Octocat`
  - Qualifiers, like `in:readme user:defunkt stars:>=1000`

> Make sure these are encoded correctly. For instance, `const queryString = 'q=' + encodeURIComponent('GitHub Octocat in:readme user:defunkt');`

### get /search/repositories

Parameters

| Name     | Type    | In     | Description                                                                                                                                                                       |
| -------- | ------- | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| accept   | string  | header | Setting to application/vnd.github.v3+json is recommended.                                                                                                                         |
| q        | string  | query  | see above.                                                                                                                                                                        |
| sort     | string  | query  | Sorts the results of your query by number of stars, forks, or help-wanted-issues or how recently the items were updated. Default: best match                                      |
| order    | string  | query  | Determines whether the first search result returned is the highest number of matches (desc) or lowest number of matches (asc). This parameter is ignored unless you provide sort. |
| per_page | integer | query  | Results per page (max 100).                                                                                                                                                       |

Pseudocode for this app:

```txt
url = https://api.github.com/repositories
headers = { Accept: "application/vnd.github.v3+json" }
stars = `stars:>=${number}`
sort = 'sort:stars'
order = 'order:desc'
perPage = 'per_page:100'
query = encode(q=stars + sort + order + per_page)
fetch(url + query, headers)
```

## GET Commits for repo

<https://docs.github.com/en/rest/reference/repos#list-commits>

Parameters

| Name     | Type    | In     | Description                                                                                                         |
| -------- | ------- | ------ | ------------------------------------------------------------------------------------------------------------------- |
| accept   | string  | header | Setting to application/vnd.github.v3+json is recommended.                                                           |
| owner    | string  | path   |                                                                                                                     |
| repo     | string  | path   |                                                                                                                     |
| since    | string  | query  | Only show notifications updated after the given time. This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ. |
| per_page | integer | query  | Results per page (max 100).                                                                                         |

```txt
inputs: owner, repo
url = https://api.github.com/repos/{owner}/{repo}/commits
headers = { Accept: "application/vnd.github.v3+json" }
since = Date - 24 hours
order = 'order:asc'
perPage = 'per_page:100'
query = encode(`q=since + order + per_page)
fetch(url + query, headers)
```
