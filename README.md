# BrightRepos

## The Exercise

Build a single-page web app that uses the Github REST API to display a list of the top 100 most starred Github repositories; including a list of the commits made in the last 24 hours.

## Running the application

See [create-react-app.md](./docs/create-react-app.md) and use `npm`, not `yarn`.

## Querying GitHub API

The returned data from the calls to GitHub's services are the backbone of this application. Unauthenticated requests are rate-limited, so calling the service minimally is crucial. See the [Github readme](./docs/github-api.md) for details.

## Architecture and Testing

[App architecture readme](./docs/architecture.md)

[Testing approaches readme](./docs/testing.md)

## Overall thoughts

Having only 3 hours, I sprinkled documentation (jsdoc, prop-types), tests (testing all scenarios), mobile-responsive styling (using flex-wrap), and UI/UX nuances throughout the application. I would expand upon these if I had a lot more time with the application.

**Thank you so much for inviting me to take this code challenge!**
